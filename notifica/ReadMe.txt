To install, move this entire folder into C:

Run "INSTALL.bat" to set your path

For information on commands type "notifica -h" into your command line.

Notifica is a cmd line utility to automatically send email alerts upon events
The argument list for Notifica is '-n', '-e', '-d', '-l', and '-h'
     -n: Create a new config file - Ex. notifica -n test_config - test_config is the name of your configuration
     -e: execute a set of configs - Ex. notifica -e test_config - reads all the test_config files and sends the appropriate email
     -d: shows the description of the config you've specified - Ex. notifica -d test_config - This would print the description to the command line
     -l: lists the configs you have made - Ex. notifica -l - Would list all of your config files
     -q: Quick send an email. No config required. - Ex. notifica -q "This is a test(Title here)" "Just testing this quicksend(Body here)" "me@mail.com(Recipients here)"
     -h: shows this helpfile