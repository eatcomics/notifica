import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import sys
import os

if "C:\\Python27" not in sys.path:
    sys.path.append("C:\\Python27")

###############
# functions
###############

# Print a help page
def Help():
    print "Notifica is a cmd line utility to automatically send email alerts upon events\n"
    print "The argument list for Notifica is '-n', '-e', '-d', '-l', and '-h'\n"
    print "     -n: Create a new config file - Ex. notifica -n test_config - test_config is the name of your configuration\n"
    print "     -e: execute a set of configs - Ex. notifica -e test_config - reads all the test_config files and sends the appropriate email\n"
    print "     -d: shows the description of the config you've specified - Ex. notifica -d test_config - This would print the description to the command line\n"
    print "     -l: lists the configs you have made - Ex. notifica -l - Would list all of your config files\n"
    print "     -q: Quick send an email. No config required. - Ex. notifica -q \"This is a test(Title here)\" \"Just testing this quicksend(Body here)\" \"me@mail.com(Recipients here)\"" 
    print "     -h: shows this helpfile\n"

# This sends the email
def Send_Email(body, subject, recipients, attachment):
    FROMADDR = "Email Here"	# Change this to your email address
    LOGIN    = FROMADDR
    PASSWORD = "Email Password Here"						# Put your password in here
    TOADDRS  = recipients						# This is who you want to send this email to (We use a maintenance email)
    SUBJ  = subject
    ATTACH = attachment

    msg = MIMEMultipart()
    msg['From'] = FROMADDR
    msg['To'] = COMMASPACE.join(TOADDRS)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = SUBJ
	
    msg.attach( MIMEText(body))

    if (ATTACH != ""):
        file = MIMEBase('application', "octet-stream")
        file.set_payload(open(ATTACH, "rb").read())
        Encoders.encode_base64(file)
        file.add_header('Content-Disposition', 'attachment; filename="' + ATTACH + '"')
        msg.attach(file)
	
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.set_debuglevel(1)
    server.ehlo()
    server.starttls()
    server.login(LOGIN, PASSWORD)
    server.sendmail(FROMADDR, TOADDRS, msg.as_string())
    server.quit()

# This generates a file for reading
def Make_File(name):
    # Description
    newDesc = open("C:\\notifica\configs\{}_description.conf".format(name), 'w+') # I wish I could say I knew why this works.
    str = raw_input("What is your description of this config? (no returns): ")
    newDesc.write(str)
    newDesc.close()

    # Subject
    newSubject = open("C:\\notifica\configs\{}_subject.conf".format(name), 'w+')
    str = raw_input('What is the subject of the email? ')
    newSubject.write(str)
    newSubject.close()

    # Body
    newBody = open("C:\\notifica\configs\{}_body.conf".format(name), 'w+')
    str = raw_input('What is the email body? ')
    newBody.write(str)
    newBody.close()

    # Recipients
    newRecipients = open("C:\\notifica\configs\{}_recipients.conf".format(name), 'w+')
    str = raw_input("Please enter the recipients separated by commas and with no spaces. ")
    newRecipients.write(str)
    newRecipients.close()

    # Attachments go here
    newAttachment = open("C:\\notifica\configs\{}_attachment.conf".format(name), 'w+')
    str = raw_input("Please enter the path of the attachment. (Blank for none) ")
    newAttachment.write(str)
    newAttachment.close()

# Reads in the configs to send the email
def Read_File(name): 
   mailBody = open("C:\\notifica\configs\{}_body.conf".format(name), 'r')
   mailSubj = open("C:\\notifica\configs\{}_subject.conf".format(name), 'r')
   mailRecip = open("C:\\notifica\configs\{}_recipients.conf".format(name), 'r')
   mailAttach = open("C:\\notifica\configs\{}_attachment.conf".format(name), 'r')

   bodyString = mailBody.read()
   subjString = mailSubj.read()
   recipString = mailRecip.read()
   recipString = recipString.split(',')
   attachString = mailAttach.read()

   mailBody.close()
   mailSubj.close()
   mailRecip.close()
   mailAttach.close()

   Send_Email(bodyString, subjString, recipString, attachString)

# Reads the description file and prints it to the command line
def Get_Description(name):
    description = open("C:\\notifica\configs\{}_description.conf".format(name), 'r')
    desc = description.read()
    description.close()
    print desc
    
# Sends an email without using the config files
def Quick_Send(subject, body, recip):
	bodyString = body
	subjString = subject
	recipString = recip
	recipString = recipString.split(',')
	
	Send_Email(bodyString, subjString, recipString)

##################
# Program Start
##################
def main():
	if (len(sys.argv) > 1):
		if str(sys.argv[1]) in ['-h']:
			Help()
		elif str(sys.argv[1]) in ['-l']:
			print os.listdir(r'configs')
		elif str(sys.argv[1]) in ['-e']:
			Read_File(str(sys.argv[2]))
		elif str(sys.argv[1]) in ['-n']:
			Make_File(str(sys.argv[2]))
		elif str(sys.argv[1]) in ['-d']:
			Get_Description(str(sys.argv[2]))
		elif str(sys.argv[1]) in ['-q']:
			Quick_Send(str(sys.argv[2]),str(sys.argv[3]),str(sys.argv[4]));
		else:
			Help()
	else:
		Help()

if __name__ == "__main__":
	main()